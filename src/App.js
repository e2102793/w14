import React from "react";
import "./App.css";

function App() {
  function Send() {
    
    let newArray = document.getElementById("chat").value;

    if(localStorage.getItem('chat') == null){
    localStorage.setItem("chat", "[]");
    }

    let oldArray = JSON.parse(localStorage.getItem("chat"));
    oldArray.push(newArray);

    localStorage.setItem("chat", JSON.stringify(oldArray));
    
    document.getElementById("chat").value=""
  }

  function Show() {
    console.log(localStorage.getItem("chat"));
  }

  return (
    <div className="App">
      <h1> W-13 </h1>
      <input type="text" id="chat" placeholder="chat"></input>
      <button className="a" onClick={Send}>
        Send
      </button>
      <br />
      <button className="b" onClick={Show}>
        Show messages
      </button>

      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Proin libero nunc
        consequat interdum varius sit amet mattis vulputate. Donec massa sapien
        faucibus et molestie ac feugiat. Purus non enim praesent elementum
        facilisis leo vel fringilla. Nibh tellus molestie nunc non blandit massa
        enim. Proin libero nunc consequat interdum varius sit amet mattis
        vulputate. Donec massa sapien faucibus et molestie ac feugiat. Purus non
        enim praesent elementum facilisis leo vel fringilla. Nibh tellus
        molestie nunc non blandit massa enim. tempor incididunt ut labore et
        dolore magna aliqua. Proin libero nunc consequat interdum varius sit
        amet mattis vulputate. Donec massa sapien faucibus et molestie ac
        feugiat. Purus non enim praesent elementum facilisis leo vel fringilla.
        Nibh tellus molestie nunc non blandit massa enim. Proin libero nunc
        consequat interdum varius sit amet mattis vulputate. Donec massa sapien
        faucibus et molestie ac feugiat. Purus non enim praesent elementum
        facilisis leo vel fringilla. Nibh tellus molestie nunc non blandit massa
        enim.
      </p>
      <p>
        Proin libero nunc consequat interdum varius sit amet mattis vulputate.
        Donec massa sapien faucibus et molestie ac feugiat. Lorem ipsum dolor
        sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
        ut labore et dolore magna aliqua. Proin libero nunc consequat interdum
        varius sit amet mattis vulputate. Donec massa sapien faucibus et
        molestie ac feugiat. Lorem ipsum dolor sit amet, consectetur adipiscing
        elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
        Proin libero nunc consequat interdum varius sit amet mattis vulputate.
        Donec massa sapien faucibus et molestie ac feugiat. tempor incididunt ut
        labore et dolore magna aliqua. Proin libero nunc consequat interdum
        varius sit amet mattis vulputate. Donec massa sapien faucibus et
        molestie ac feugiat. Purus non enim praesent elementum facilisis leo vel
        fringilla. Nibh tellus molestie nunc non blandit massa enim. Proin
        libero nunc consequat interdum varius sit amet mattis vulputate. Donec
        massa sapien faucibus et molestie ac feugiat. Purus non enim praesent
        elementum facilisis leo vel fringilla. Nibh tellus molestie nunc non
        blandit massa enim. tempor incididunt ut labore et dolore magna aliqua.
        Proin libero nunc consequat interdum varius sit amet mattis vulputate.
        Donec massa sapien faucibus et molestie ac feugiat. Purus non enim
        praesent elementum facilisis leo vel fringilla. Nibh tellus molestie
        nunc non blandit massa enim. Proin libero nunc consequat interdum varius
        sit amet mattis vulputate. Donec massa sapien faucibus et molestie ac
        feugiat. Purus non enim praesent elementum facilisis leo vel fringilla.
        Nibh tellus molestie nunc non blandit massa enim.
      </p>
      <p>
        Purus non enim praesent elementum facilisis leo vel fringilla. Nibh
        tellus molestie nunc non blandit massa enim. Lorem ipsum dolor sit amet,
        consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
        et dolore magna aliqua. Proin libero nunc consequat interdum varius sit
        amet mattis vulputate. Donec massa sapien faucibus et molestie ac
        feugiat. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
        eiusmod tempor incididunt ut labore et dolore magna aliqua. Proin libero
        nunc consequat interdum varius sit amet mattis vulputate. Donec massa
        sapien faucibus et molestie ac feugiat. tempor incididunt ut labore et
        dolore magna aliqua. Proin libero nunc consequat interdum varius sit
        amet mattis vulputate. Donec massa sapien faucibus et molestie ac
        feugiat. Purus non enim praesent elementum facilisis leo vel fringilla.
        Nibh tellus molestie nunc non blandit massa enim. Proin libero nunc
        consequat interdum varius sit amet mattis vulputate. Donec massa sapien
        faucibus et molestie ac feugiat. Purus non enim praesent elementum
        facilisis leo vel fringilla. Nibh tellus molestie nunc non blandit massa
        enim. tempor incididunt ut labore et dolore magna aliqua. Proin libero
        nunc consequat interdum varius sit amet mattis vulputate. Donec massa
        sapien faucibus et molestie ac feugiat. Purus non enim praesent
        elementum facilisis leo vel fringilla. Nibh tellus molestie nunc non
        blandit massa enim. Proin libero nunc consequat interdum varius sit amet
        mattis vulputate. Donec massa sapien faucibus et molestie ac feugiat.
        Purus non enim praesent elementum facilisis leo vel fringilla. Nibh
        tellus molestie nunc non blandit massa enim.
      </p>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor
        sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
        ut labore et dolore magna aliqua. Proin libero nunc consequat interdum
        varius sit amet mattis vulputate. Donec massa sapien faucibus et
        molestie ac feugiat. Lorem ipsum dolor sit amet, consectetur adipiscing
        elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
        Proin libero nunc consequat interdum varius sit amet mattis vulputate.
        Donec massa sapien faucibus et molestie ac feugiat. tempor incididunt ut
        labore et dolore magna aliqua. Proin libero nunc consequat interdum
        varius sit amet mattis vulputate. Donec massa sapien faucibus et
        molestie ac feugiat. Purus non enim praesent elementum facilisis leo vel
        fringilla. Nibh tellus molestie nunc non blandit massa enim. Proin
        libero nunc consequat interdum varius sit amet mattis vulputate. Donec
        massa sapien faucibus et molestie ac feugiat. Purus non enim praesent
        elementum facilisis leo vel fringilla. Nibh tellus molestie nunc non
        blandit massa enim. tempor incididunt ut labore et dolore magna aliqua.
        Proin libero nunc consequat interdum varius sit amet mattis vulputate.
        Donec massa sapien faucibus et molestie ac feugiat. Purus non enim
        praesent elementum facilisis leo vel fringilla. Nibh tellus molestie
        nunc non blandit massa enim. Proin libero nunc consequat interdum varius
        sit amet mattis vulputate. Donec massa sapien faucibus et molestie ac
        feugiat. Purus non enim praesent elementum facilisis leo vel fringilla.
        Nibh tellus molestie nunc non blandit massa enim.
      </p>
      <p>
        Proin libero nunc consequat interdum varius sit amet mattis vulputate.
        Donec massa sapien faucibus et molestie ac feugiat. Lorem ipsum dolor
        sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
        ut labore et dolore magna aliqua. Proin libero nunc consequat interdum
        varius sit amet mattis vulputate. Donec massa sapien faucibus et
        molestie ac feugiat. Lorem ipsum dolor sit amet, consectetur adipiscing
        elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
        Proin libero nunc consequat interdum varius sit amet mattis vulputate.
        Donec massa sapien faucibus et molestie ac feugiat. Proin libero nunc
        consequat interdum varius sit amet mattis vulputate. Donec massa sapien
        faucibus et molestie ac feugiat. Lorem ipsum dolor sit amet, consectetur
        adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
        magna aliqua. Proin libero nunc consequat interdum varius sit amet
        mattis vulputate. Donec massa sapien faucibus et molestie ac feugiat.
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Proin libero nunc
        consequat interdum varius sit amet mattis vulputate. Donec massa sapien
        faucibus et molestie ac feugiat. Proin libero nunc consequat interdum
        varius sit amet mattis vulputate. Donec massa sapien faucibus et
        molestie ac feugiat. Lorem ipsum dolor sit amet, consectetur adipiscing
        elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
        Proin libero nunc consequat interdum varius sit amet mattis vulputate.
        Donec massa sapien faucibus et molestie ac feugiat. Lorem ipsum dolor
        sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
        ut labore et dolore magna aliqua. Proin libero nunc consequat interdum
        varius sit amet mattis vulputate. Donec massa sapien faucibus et
        molestie ac feugiat. Proin libero nunc consequat interdum varius sit
        amet mattis vulputate. Donec massa sapien faucibus et molestie ac
        feugiat. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
        eiusmod tempor incididunt ut labore et dolore magna aliqua. Proin libero
        nunc consequat interdum varius sit amet mattis vulputate. Donec massa
        sapien faucibus et molestie ac feugiat. Lorem ipsum dolor sit amet,
        consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
        et dolore magna aliqua. Proin libero nunc consequat interdum varius sit
        amet mattis vulputate. Donec massa sapien faucibus et molestie ac
        feugiat.
      </p>
    </div>
  );
}

export default App;
